package com.android.movieapp.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.android.movieapp.TestData
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class MoviesDaoTest : AppDatabaseTest() {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun insertMoviesAndRead() = runTest {
        db.MoviesDao().insertAllMovies(TestData.movieList)

        val movies = db.MoviesDao().getMovieList()
        assertThat(movies).isEqualTo(TestData.movieList)
    }

    @Test
    fun insertMovieDetailAndRead() = runTest {
        db.MoviesDao().insertMovieDetails(TestData.movie2)
        val movie =
            db.MoviesDao().getMovieDetails(TestData.movie2.id)
        assertThat(movie).isEqualTo(TestData.movie2)
    }
}