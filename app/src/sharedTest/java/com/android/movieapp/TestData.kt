package com.android.movieapp

import com.android.movieapp.data.model.Movie

object TestData {
    val movie1 = Movie(1,"foo", "bar","test","descr")
    val movie2 = Movie(2,"foo 2", "bar 2","test 2","descr 2")

    val movieList = listOf(movie1, movie2)
}