package com.android.movieapp.data.movie

import com.android.movieapp.TestData
import com.android.movieapp.data.api.FeedMoviesResponse
import com.android.movieapp.data.api.MovieService
import com.android.movieapp.data.api.MovieService.Companion.API_KEY
import com.android.movieapp.data.api.Result
import com.android.movieapp.data.db.MoviesDao
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class FeedMoviesRepositoryTest {
    private lateinit var dao: MoviesDao
    private lateinit var service: MovieService
    private lateinit var repository: FeedMoviesRepositoryImpl

    @Before
    fun init() {
        dao = mock(MoviesDao::class.java)
        service = mock(MovieService::class.java)
        repository = FeedMoviesRepositoryImpl(dao, service, Dispatchers.IO)
    }

    @Test
    fun getFeedMovies_emptyLocalData_retrieveListRemote() = runTest {
        `when`(dao.getMovieList()).thenReturn(emptyList())
        `when`(service.getMovieList(API_KEY, 1))
            .thenReturn(FeedMoviesResponse(TestData.movieList))

        val movies = repository.getFeedMovieList().first()
        assertThat(movies is Result.Success)
        if (movies is Result.Success) {
            assertThat(movies.data).isEqualTo(TestData.movieList)
        }
        verify(dao).insertAllMovies(TestData.movieList)
    }

    @Test
    fun getFeedMovies_withLocalData_movieListRetrievedRemoteAndSavedLocally() = runTest {
        `when`(dao.getMovieList()).thenReturn(TestData.movieList)

        `when`(service.getMovieList(API_KEY, 1)).thenReturn(
            FeedMoviesResponse(listOf(TestData.movie1))
        )

        val repository = repository.getFeedMovieList()
        val movieList = repository.first()
        verify(dao).getMovieList()
        assertThat(movieList is Result.Success)
        if (movieList is Result.Success) {
            assertThat(movieList.data).isEqualTo(TestData.movieList)
        }

        val movieListRemote = repository.last()
        verify(service, times(2)).getMovieList(API_KEY, 1)
        assertThat(movieListRemote is Result.Success)
        if (movieListRemote is Result.Success) {
            assertThat(movieListRemote.data).isEqualTo(listOf(TestData.movie1))
        }
        verify(dao, times(2)).insertAllMovies(listOf(TestData.movie1))
    }

    @Test
    fun getFeedMovies_error() = runTest {
        `when`(dao.getMovieList()).thenReturn(TestData.movieList)
        `when`(service.getMovieList( API_KEY,1)).thenThrow(
            RuntimeException("Runtime exception")
        )

        val movieList = repository.getFeedMovieList().first()
        verify(dao).getMovieList()
        assertThat(movieList is Result.Success)
        if (movieList is Result.Success) {
            assertThat(movieList.data).isEqualTo(TestData.movieList)
        }
        assertThat(movieList is Result.Error)
        if (movieList is Result.Error) {
            assertThat(movieList.exception.message).isEqualTo("Runtime exception")
        }
        verify(dao, never()).insertAllMovies(TestData.movieList)
    }
}