package com.android.movieapp.data.movie

import com.android.movieapp.data.api.MovieService
import com.android.movieapp.data.api.MovieService.Companion.API_KEY
import com.android.movieapp.data.api.Result
import com.android.movieapp.data.db.MoviesDao
import com.android.movieapp.TestData
import com.google.common.truth.Truth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

@OptIn(ExperimentalCoroutinesApi::class)
class MovieDetailsRepositoryTest {
    private lateinit var repository: MovieDetailsRepositoryImpl
    private lateinit var service: MovieService
    private lateinit var dao: MoviesDao

    @Before
    fun setup() {
        service = mock(MovieService::class.java)
        dao = mock(MoviesDao::class.java)
        repository = MovieDetailsRepositoryImpl(dao, service, Dispatchers.Unconfined)
    }

    @Test
    fun getMovieDetails_withoutLocalData_movieIsRetrievedRemote() = runTest {
        `when`(service.getMovieDetails(1, API_KEY)).thenReturn(
            TestData.movie1
        )

        val movie = repository.getMovieDetails(1).first()

        Truth.assertThat(movie is Result.Success)
        if (movie is Result.Success) {
            Truth.assertThat(movie.data).isEqualTo(TestData.movie1)
        }
        verify(dao).insertMovieDetails(TestData.movie1)
    }

    @Test
    fun getMovieDetails_withLocalData_movieIsRetrievedRemoteAndSavedLocally() = runTest {
        `when`(dao.getMovieDetails(2)).thenReturn(TestData.movie2)

        val movie2 = TestData.movie2.copy(title = "updated title")
        `when`(service.getMovieDetails(2, API_KEY)).thenReturn(
            movie2
        )

        val repository = repository.getMovieDetails(2)
        val movie = repository.first()
        verify(dao).getMovieDetails(2)
        Truth.assertThat(movie is Result.Success)
        if (movie is Result.Success) {
            Truth.assertThat(movie.data).isEqualTo(TestData.movie2)
        }

        val movieRemote = repository.last()
        verify(service, times(2)).getMovieDetails(2, API_KEY)
        Truth.assertThat(movieRemote is Result.Success)
        if (movieRemote is Result.Success) {
            Truth.assertThat(movieRemote.data).isEqualTo(movie2)
        }
        verify(dao, times(2)).insertMovieDetails(movie2)
    }

    @Test
    fun getMovieDetails_error() = runTest {
        `when`(dao.getMovieDetails(1)).thenReturn(TestData.movie1)
        `when`(service.getMovieDetails(1, API_KEY)).thenThrow(
            RuntimeException("Runtime exception")
        )

        val movie = repository.getMovieDetails(1).first()
        verify(dao).getMovieDetails(1)
        Truth.assertThat(movie is Result.Success)
        if (movie is Result.Success) {
            Truth.assertThat(movie.data).isEqualTo(TestData.movie1)
        }
        Truth.assertThat(movie is Result.Error)
        if (movie is Result.Error) {
            Truth.assertThat(movie.exception.message).isEqualTo("Runtime exception")
        }
        verify(dao, never()).insertMovieDetails(TestData.movie1)
    }
}