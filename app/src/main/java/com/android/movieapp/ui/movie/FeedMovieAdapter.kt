package com.android.movieapp.ui.movie

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.movieapp.databinding.ListItemMovieBinding
import com.android.movieapp.data.model.Movie

class FeedMovieAdapter :
    ListAdapter<Movie, FeedMovieAdapter.FeedMovieViewHolder>(PlantDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedMovieViewHolder {
        return FeedMovieViewHolder(
            ListItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: FeedMovieViewHolder, position: Int) {
        val feedMovieOffer = getItem(position)
        holder.apply {
            bind(createOnClickListener(feedMovieOffer.id), feedMovieOffer)
        }
    }

    class FeedMovieViewHolder(
        private val binding: ListItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Movie) {
            binding.apply {
                clickListener = listener
                feedMovie = item
                executePendingBindings()
            }
        }
    }

    private fun createOnClickListener(id: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                FeedMoviesFragmentDirections.actionFeedMoviesFragmentToMovieDetailsFragment(id)
            it.findNavController().navigate(direction)
        }
    }
}

private class PlantDiffCallback : DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}