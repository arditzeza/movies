package com.android.movieapp.ui.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.movieapp.data.api.Result
import com.android.movieapp.data.model.Movie
import com.android.movieapp.data.movie.FeedMoviesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedMovieViewModel @Inject constructor(private val repository: FeedMoviesRepository) :
    ViewModel() {
    private val _movieList = MutableStateFlow<List<Movie>>(emptyList())
    private val _error = MutableStateFlow<String?>(null)

    val movieList: StateFlow<List<Movie>>
        get() = _movieList
    val error: StateFlow<String?>
        get() = _error

    fun getMovieList() {
        viewModelScope.launch {
            repository.getFeedMovieList().collect { result ->
                if (result is Result.Success) {
                    _movieList.value = result.data
                } else if (result is Result.Error) {
                    result.exception.message?.let {
                        _error.value = it
                    }
                }
            }
        }
    }

    fun onErrorShown() {
        _error.value = null
    }
}