package com.android.movieapp.ui.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.RecyclerView
import com.android.movieapp.data.model.Movie
import com.android.movieapp.databinding.FragmentFeedMoviesBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FeedMoviesFragment : Fragment() {

    private lateinit var binding: FragmentFeedMoviesBinding
    private val feedMovieViewModel: FeedMovieViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedMoviesBinding.inflate(inflater, container, false)
        binding.feedMovieRecycler

        feedMovieViewModel.getMovieList()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    feedMovieViewModel.movieList.collect {
                        showFeedMovies(binding.feedMovieRecycler, it)
                    }
                }

                launch {
                    feedMovieViewModel.error.collect {
                        it?.let {
                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show()
                            feedMovieViewModel.onErrorShown()
                        }
                    }
                }
            }
        }
    }

    private fun showFeedMovies(recyclerView: RecyclerView, list: List<Movie>) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = FeedMovieAdapter()
        }
        (recyclerView.adapter as FeedMovieAdapter).submitList(list)
    }
}