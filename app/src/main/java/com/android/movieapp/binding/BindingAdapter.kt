package com.android.movieapp.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.movieapp.data.api.MovieService.Companion.BASE_IMAGE_URL
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(BASE_IMAGE_URL + imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}