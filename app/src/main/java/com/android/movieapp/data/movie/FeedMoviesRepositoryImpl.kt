package com.android.movieapp.data.movie

import com.android.movieapp.data.api.MovieService
import com.android.movieapp.data.api.MovieService.Companion.API_KEY
import com.android.movieapp.data.api.Result
import com.android.movieapp.data.db.MoviesDao
import com.android.movieapp.data.model.Movie
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface FeedMoviesRepository {
    fun getFeedMovieList(): Flow<Result<List<Movie>>>
}

class FeedMoviesRepositoryImpl @Inject constructor(
    private val dao: MoviesDao,
    private val movieService: MovieService,
    private val provideIoDispatcher: CoroutineDispatcher
) : FeedMoviesRepository {
    override fun getFeedMovieList() = flow {
        val movieList = dao.getMovieList()
        if (movieList.isNotEmpty()) {
            emit(Result.Success(movieList))
        }
        try {
            val movies = movieService.getMovieList(API_KEY, 1).results
            emit(Result.Success(movies))
            dao.insertAllMovies(movies)
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }.flowOn(provideIoDispatcher)
}