package com.android.movieapp.data.movie

import com.android.movieapp.data.api.MovieService
import com.android.movieapp.data.api.MovieService.Companion.API_KEY
import com.android.movieapp.data.api.Result
import com.android.movieapp.data.db.MoviesDao
import com.android.movieapp.data.model.Movie
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface MovieDetailsRepository {
    fun getMovieDetails(movieId: Int): Flow<Result<Movie>>
}

class MovieDetailsRepositoryImpl @Inject constructor(
    private val dao: MoviesDao,
    private val service: MovieService,
    private val provideIoDispatcher: CoroutineDispatcher
) : MovieDetailsRepository {

    override fun getMovieDetails(movieId: Int) = flow {
        val movie = dao.getMovieDetails(movieId)
        if (movie != null) {
            emit(Result.Success(movie))
        }
        try {
            service.getMovieDetails(movieId, API_KEY)?.let {
                emit(Result.Success(it))
                dao.insertMovieDetails(it)
            } ?: emit(Result.Error(Exception("No Data")))
        } catch (e: Exception) {
            emit(Result.Error(Exception(e.message)))
        }
    }.flowOn(provideIoDispatcher)
}