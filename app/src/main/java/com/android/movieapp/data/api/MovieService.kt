package com.android.movieapp.data.api

import com.android.movieapp.data.model.Movie
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MovieService {

    @GET("movie/top_rated")
    suspend fun getMovieList(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): FeedMoviesResponse<Movie>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(
        @Path("movie_id") id: Int,
        @Query("api_key") apiKey: String
    ): Movie?

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val API_KEY = "d13430285a5e48c0e10532e9e3914cab"
        const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original/"
    }
}