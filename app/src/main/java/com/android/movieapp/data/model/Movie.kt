package com.android.movieapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "feedMovie")
data class Movie(
    @PrimaryKey
    @field:SerializedName("id")
    val id: Int,
    val title: String,
    @field:SerializedName("poster_path")
    val image: String,
    @field:SerializedName("backdrop_path")
    val backImage: String,
    val overview: String
)




