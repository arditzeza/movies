package com.android.movieapp.data.db

import androidx.room.*
import com.android.movieapp.data.model.Movie

@Dao
interface MoviesDao {

    @Query("SELECT * FROM feedMovie")
    suspend fun getMovieList(): List<Movie>

    @Transaction
    @Query("SELECT * FROM feedMovie WHERE id = :id")
    suspend fun getMovieDetails(id: Int): Movie?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllMovies(homeFeedMovies: List<Movie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieDetails(homeFeedMovies: Movie)
}