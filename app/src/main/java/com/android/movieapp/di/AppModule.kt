package com.android.movieapp.di

import com.android.movieapp.data.api.MovieService
import com.android.movieapp.data.db.MoviesDao
import com.android.movieapp.data.movie.FeedMoviesRepository
import com.android.movieapp.data.movie.FeedMoviesRepositoryImpl
import com.android.movieapp.data.movie.MovieDetailsRepository
import com.android.movieapp.data.movie.MovieDetailsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    fun provideMovieDetailsRepository(
        dao: MoviesDao,
        service: MovieService,
        ioDispatcher: CoroutineDispatcher
    ): MovieDetailsRepository = MovieDetailsRepositoryImpl(dao, service, ioDispatcher)

    @Provides
    fun provideFeedMovieRepository(
        dao: MoviesDao,
        service: MovieService,
        ioDispatcher: CoroutineDispatcher
    ): FeedMoviesRepository = FeedMoviesRepositoryImpl(dao, service, ioDispatcher)
}
